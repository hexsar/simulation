import os
import subprocess
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument, ExecuteProcess
from launch.substitutions import LaunchConfiguration, Command

'''
Para modificar las coordenadas de spawn, se puede proporcionar los valores de las coordenadas como argumentos. Por ejemplo:

ros2 launch one_leg_simulation one_leg_simulation.launch.py spawn_x:=1.0 spawn_y:=2.0 spawn_z:=0.5

'''

def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')

    # Obtén la ruta del paquete "one_leg_simulation"
    simulation_pkg_dir = get_package_share_directory('one_leg_simulation')

    # Ruta del mundo vacío de Gazebo
    empty_world = os.path.join(simulation_pkg_dir, 'worlds', 'empty_world.world')

    # Verifica si el mundo vacío de Gazebo existe
    if not os.path.exists(empty_world):
        print("ERROR: El mundo vacío de Gazebo no se encuentra en la ruta especificada.")
        return LaunchDescription()

    # Obtén la ruta del paquete "one_leg"
    one_leg_pkg_dir = get_package_share_directory('one_leg')

    # Ruta del archivo Xacro
    xacro_file = os.path.join(one_leg_pkg_dir, 'urdf', 'one_leg.xacro')

    # Ruta del archivo URDF convertido
    urdf_file = os.path.join(one_leg_pkg_dir, 'urdf', 'one_leg.urdf')

    # Comando para convertir el archivo Xacro a URDF
    convert_cmd = f'xacro {xacro_file} > {urdf_file}'

    # Ejecuta el comando de conversión utilizando el nodo ExecuteProcess
    convertion_cmd = ExecuteProcess(
        cmd=['bash', '-c', convert_cmd],
        output='screen'
    )

    # Coordenadas del spawn del modelo en Gazebo
    spawn_x = LaunchConfiguration('spawn_x', default='0.0')
    spawn_y = LaunchConfiguration('spawn_y', default='0.0')
    spawn_z = LaunchConfiguration('spawn_z', default='0.1')

    # Lanza el nodo para iniciar Gazebo
    gzserver_cmd = ExecuteProcess(
        cmd=['gazebo', '--verbose', '-s', 'libgazebo_ros_factory.so','--pause', empty_world],
        output='screen'
    )

    # Lanza el nodo para cargar el archivo xacro en Gazebo
    spawn_entity_cmd = ExecuteProcess(
        cmd=['ros2', 'run', 'gazebo_ros', 'spawn_entity.py', '-entity', 'one_leg', '-file', urdf_file,'-x', spawn_x, '-y', spawn_y, '-z', spawn_z],
        output='screen'
    )

    # Declare launch arguments
    robot_description_arg = DeclareLaunchArgument(
        'robot_description',
        default_value=urdf_file,
        description='Path to the robot model XML file'
    )

    joint_states_remap = DeclareLaunchArgument(
        'joint_states',
        default_value='different_joint_states',
        description='Remap joint_states topic'
    )

    with open(urdf_file, 'r') as infp:
        robot_desc = infp.read()

    bridge = Node(
        package='ros_gz_bridge',
        executable='parameter_bridge',
        arguments=[
            # Clock (IGN -> ROS2)
            '/clock@rosgraph_msgs/msg/Clock[gz.msgs.Clock',
            # Joint states (IGN -> ROS2)
            '/world/empty/model/one_leg/joint_state@sensor_msgs/msg/JointState[gz.msgs.Model',
        ],
        remappings=[
            ('/world/empty/model/one_leg/joint_state', 'joint_states'),
        ],
        output='screen'
    )

    return LaunchDescription([
        convertion_cmd,
        robot_description_arg,
        joint_states_remap,
        gzserver_cmd,
        spawn_entity_cmd,
        DeclareLaunchArgument('spawn_x', default_value='0.0', description='Spawn coordinate X'),
        DeclareLaunchArgument('spawn_y', default_value='0.0', description='Spawn coordinate Y'),
        DeclareLaunchArgument('spawn_z', default_value='0.0', description='Spawn coordinate Z'),
        bridge,
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time, 'robot_description': robot_desc}],
            arguments=[urdf_file]),
        ])

