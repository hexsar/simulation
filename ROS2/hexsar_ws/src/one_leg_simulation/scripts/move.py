import rclpy
from rclpy.node import Node
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint


class JointController(Node):
    def init(self):
        super().init('joint_controller')
        self.publisher = self.create_publisher(JointTrajectory, '/joint_trajectory', 10)
        self.timer = self.create_timer(1.0, self.send_joint_commands)

    def send_joint_commands(self):
        # Create a JointTrajectory message
        joint_trajectory = JointTrajectory()
        joint_trajectory.joint_names = ['AR_coxa_joint', 'AR_femur_joint', 'AR_tibia_joint']

        # Create a JointTrajectoryPoint with desired joint positions
        joint_positions = [1.0, 2.0, 3.0]  # Replace with your desired joint positions
        trajectory_point = JointTrajectoryPoint()
        trajectory_point.positions = joint_positions
        trajectory_point.time_from_start.sec = 1  # Set the desired time for the movement

        # Add the trajectory point to the JointTrajectory message
        joint_trajectory.points.append(trajectory_point)

        # Publish the JointTrajectory message
        self.publisher.publish(joint_trajectory)


def main(args=None):
    rclpy.init(args=args)
    joint_controller = JointController()
    rclpy.spin(joint_controller)
    joint_controller.destroy_node()
    rclpy.shutdown()


if name == 'main':
    main()
