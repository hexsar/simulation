# simulation

Docker image for Gazebo simulator

## How to use
Simulation availables are:

 - Visualization and urdf testing
 - Easy controller test whit IK
 - Run leg simulation with controller ( no inverse kinematic)
 - Run full body controller (Syropod)

### Visualization and urdf testing
Display a full robot model in RViz

1. Pull Docker image with:
```bash
cd path-to-repository
docker run -v ${PWD}/catkin_ws:/home/ubuntu/catkin_ws -p 6080:80 --shm-size=512m tiryoh/ros-desktop-vnc:noetic
```
2. Browse http://127.0.0.1:6080/.

3. Open a new terminal (through the web server) and type:

```bash
cd catkin_ws
sudo apt update
sudo apt install ros-noetic-effort-controllers
catkin build
source devel/setup.bash
roslaunch full_robot_description display.launch
```

### Easy controller test whit IK
The controller is a simple program whit which we can send angles for the coxa, tibia and femur.

1. Pull Docker image with:
```bash
cd path-to-repository
docker run -v ${PWD}/catkin_ws:/home/ubuntu/catkin_ws -p 6080:80 --shm-size=512m tiryoh/ros-desktop-vnc:noetic
```
2. Browse http://127.0.0.1:6080/.

3. In the terminal of step 3 of "Run leg simulation with controller". you ejecute:

```bash
cd ../src/
python3 send_leg_position.py
```
4. Try sending angles, for example:
```bash
get coxa angle: 0.5  # <-- value sending
coxa : 0.5           
get tibia angle: 0.5 # <-- value sending
tibia : 0.5          
get femur angle: 0.5 # <-- value sending
femur : 0.5        
```
This program is for quick testing. For that reason it is not very cute =)

### Run leg simulation with controller

1. Pull Docker image with:
```bash
cd path-to-repository
docker run -v ${PWD}/catkin_ws:/home/ubuntu/catkin_ws -p 6080:80 --shm-size=512m tiryoh/ros-desktop-vnc:noetic
```
2. Browse http://127.0.0.1:6080/.

3. Open a new terminal (through the web server) and type:

```bash
cd catkin_ws
sudo apt update
sudo apt install ros-noetic-effort-controllers
catkin build
source devel/setup.bash
roslaunch leg_description run_leg_simulation.launch #in the new terminal
```

4. Charge tibia controller.

Now open a new terminal in devel without closing the simulation and type:

```bash
source setup.bash
roslaunch simple_leg_control simple_leg_control.launch
```
5. Send a position to a specific joint.

Finally, open the last terminal without closing the other two.

```bash
source setup.bash
rostopic pub -1 /simple_leg/tibia_joint_position_controller/command std_msgs/Float64 "data: 1.4"
#Send a number with "data: N°", for move the tibia.
```
This simulation does not have an inverse kinematic calculation.

### Run full body controller (Syropod)

1. Create a new folder `hexsar_ws` for the workspace
```bash
mkdir -p hexsar_ws/src
```
2. Clone syropod controller on `hexsar branch`, and HexSar simulation
```bash
cd hexsar_ws/src
git clone https://github.com/MartinNievas/syropod_highlevel_controller.git
git clone git@gitlab.com:hexsar/simulation.git
git clone https://github.com/MartinNievas/fake_key_joy.git
git clone https://github.com/csiro-robotics/syropod_remote.git
cd ../..
```

3. Run the container
- `docker run -v ${PWD}/hexsar_ws:/home/ubuntu/catkin_ws -p 6080:80 --shm-size=512m tiryoh/ros-desktop-vnc:noetic
`
3. Browse http://127.0.0.1:6080/.

4. Open a new terminal (through the web server) and type:

```bash
cd catkin_ws
catkin build
source devel/setup.bash
roslaunch full_robot_description run_full_robot_simulation_syropod.launch rviz:=true
```
Press play button on the gazebo window.
Open a new terminal (through the web server) and type:
```bash
rosrun fake_key_joy fake_joy.py
```
In order to start the controller, in the fake_joy console press:
```
(s)->(d)->(d)
(i)
```

Hexa should start moving.


Thanks [Tiryoh](https://github.com/Tiryoh) for the [Docker image](https://github.com/Tiryoh/docker-ros-desktop-vnc)
