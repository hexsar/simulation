# simulation

Docker image for Gazebo simulator

## How to use

1. Pull Docker image with:
```bash
cd path-to-repository
docker run -v ${PWD}/catkin_ws:/home/ubuntu/catkin_ws -p 6080:80 --shm-size=512m tiryoh/ros-desktop-vnc:noetic
```
2. Browse http://127.0.0.1:6080/.

## View full robot example
Open a new terminal (through the web server) and type:

```bash
cd catkin_ws
sudo apt update
sudo apt install ros-noetic-effort-controllers
catkin build
source devel/setup.bash
roslaunch full_robot_description display.launch
```

Thanks [Tiryoh](https://github.com/Tiryoh) for the [Docker image](https://github.com/Tiryoh/docker-ros-desktop-vnc)


# Easy controller test whit IK
The controller is a simple program whit which we can send angles for the coxa, tibia and femur.  

## How to use  
1. You already need every steps of [here](https://gitlab.com/hexsar/simulation) in "Run leg simulation with controller". 

2. In the terminal of step 3 of "Run leg simulation with controller". you ejecute:

```bash
cd ../src/
python3 send_leg_position.py
```
3. Try sending angles, for example: 
```bash
get coxa angle: 0.5  # <-- value sending 
coxa : 0.5           
get tibia angle: 0.5 # <-- value sending 
tibia : 0.5          
get femur angle: 0.5 # <-- value sending 
femur : 0.5        
```
This program is for quick testing. For that reason it is not very cute =)

## Run leg simulation with controller

The first step for test the simulation, it's up here.   
We need to have done these steps, minus the last one (don't run "roslaunch ...").

Well, now the steps of test: 

1. Open a Gazebo simulation.

```bash
cd devel
source setup.bash
#Open new termnial in devel.
roslaunch leg_description run_leg_simulation.launch #in the new terminal
```
2. Charge tibia controller.

Now open a new terminal in devel without closing the simulation.  

```bash
source setup.bash
roslaunch simple_leg_control simple_leg_control.launch
```
3. Send a position

Finally, open the last terminal without closing the other two.

```bash
source setup.bash
rostopic pub -1 /simple_leg/tibia_joint_position_controller/command std_msgs/Float64 "data: 1.4"
#Send a number with "data: N°", for move the tibia.
```
This simulation does not have an inverse kinematic calculation.
