import os
import subprocess


while (1):
    file = open("conf_movement.sh","w")

    coxa=input("get coxa angle: ")
    print("coxa : "+coxa)
    tibia=input("get tibia angle: ")
    print("tibia : "+tibia)
    femur=input("get femur angle: ")
    print("femur : "+femur)
    
    file.write("#! /bin/bash "+ os.linesep)
    file.write("rostopic pub -1 /simple_leg/coxa_joint_position_controller/command std_msgs/Float64 \"data: " +coxa)
    file.write(" \" ")
    file.write("& rostopic pub -1 /simple_leg/tibia_joint_position_controller/command std_msgs/Float64 \"data: " +tibia)
    file.write(" \" ")
    file.write("& rostopic pub -1 /simple_leg/femur_joint_position_controller/command std_msgs/Float64 \"data: " +femur)
    file.write(" \" ")
    file.close()
    subprocess.call('./eject_config.sh')
