#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
import math
import sys
from inverse_kinematic import IK_leg

class LegsController:
    """leg controller"""
    def __init__(self):
        self.position_x = 40.0
        self.position_y = 40.0
        self.position_z = -40.0

        self.tibia_angle = 0.0
        self.coxa_angle = 0.0
        self.femur_angle = 0.0

        self.tibia_rad = 0.0
        self.coxa_rad = 0.0
        self.femur_rad = 0.0

        self.talker()

    def get_joint_pose(self, x, y, z):
        """Receive desider position in m and compute joint angles

            x: x target position
            y: y target position
            z: z target position
        """
        # IK_Leg give us a list of size 3, 0 is coxa,femur 1 and tibia is 2.
        [self.coxa_angle, self.femur_angle, self.tibia_angle] = IK_leg([x_radian, y_radian, z_radian], 1)
        #Convert angle x,y,z from degrees to radians
        self.coxa_rad = math.radians(self.coxa_angle)
        self.femur_rad = math.radians(self.femur_angle)
        self.tibia_rad = math.radians(self.tibia_angle)

    def talker(self):
        # This is only for one joint, coxa and femur must be added
        self.coxa_joint_publisher= rospy.Publisher('/simple_leg/coxa_joint_position_controller/command', Float64, queue_size=10)
        self.femur_joint_publisher= rospy.Publisher('/simple_leg/femur_joint_position_controller/command', Float64, queue_size=10)
        self.tibia_joint_publisher= rospy.Publisher('/simple_leg/tibia_joint_position_controller/command', Float64, queue_size=10)

        rate = rospy.Rate(10) # 10hz
        while not rospy.is_shutdown():

            self.get_joint_pose(self.position_x, self.position_y, self.position_z)

            rospy.loginfo("%f, %f, %f", self.coxa_angle, self.femur_angle, self.tibia_angle)
            rospy.loginfo("%f, %f, %f", self.coxa_rad, self.femur_rad, self.tibia_rad)

            # Here we send the joint angle in radians to the robot
            self.coxa_joint_publisher.publish(self.coxa_rad)
            self.femur_joint_publisher.publish(self.femur_rad)
            self.tibia_joint_publisher.publish(self.tibia_rad)

            rate.sleep()

def main(args):
    try:
        rospy.init_node('legs_controller', anonymous=True)
        legs_controller = LegsController()
    except KeyboardInterrupt:
      print("Shutting down")

if __name__ == '__main__':
    main(sys.argv)
